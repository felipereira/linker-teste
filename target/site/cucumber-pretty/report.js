$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("02_00_CadastroUsuarioComumPapeisTest.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 4,
  "name": "Cadastro de usuario comum com papel Controle de Jornada",
  "description": "",
  "id": "cadastro-de-usuario-comum-com-papel-controle-de-jornada",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 3,
      "name": "@CadastroUsuarioPapel"
    }
  ]
});
formatter.scenario({
  "line": 6,
  "name": "Criar um usuario comum com papeis",
  "description": "",
  "id": "cadastro-de-usuario-comum-com-papel-controle-de-jornada;criar-um-usuario-comum-com-papeis",
  "type": "scenario",
  "keyword": "Cenario"
});
formatter.step({
  "line": 8,
  "name": "o usuario entrar no endereco",
  "keyword": "Dado "
});
formatter.step({
  "line": 10,
  "name": "ele logar no sistema com o login correto",
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "o usuario podera criar um usuario com papel \"Controle de Jornada\" entrando no menu lateral Papeis/Incluir",
  "keyword": "Entao "
});
formatter.match({
  "location": "CadastroUsuarioComumPapeisTest.configAmbiente()"
});
formatter.result({
  "duration": 35487666541,
  "status": "passed"
});
formatter.match({
  "location": "CadastroUsuarioComumPapeisTest.acessoPortal()"
});
formatter.result({
  "duration": 4907685225,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"name\",\"selector\":\"j_username\"}\n  (Session info: chrome\u003d61.0.3163.100)\n  (Driver info: chromedriver\u003d2.32.498550 (9dec58e66c31bcc53a9ce3c7226f0c1c5810906a),platform\u003dWindows NT 10.0.10240 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 55 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.4.0\u0027, revision: \u0027unknown\u0027, time: \u0027unknown\u0027\nSystem info: host: \u0027OMNI-2438\u0027, ip: \u002710.40.1.75\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.32.498550 (9dec58e66c31bcc53a9ce3c7226f0c1c5810906a), userDataDir\u003dC:\\Users\\FELIPE~1.PER\\AppData\\Local\\Temp\\scoped_dir11260_27704}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d61.0.3163.100, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, setWindowRect\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 87342a9f286ecab397242cc4f6f7168f\n*** Element info: {Using\u003dname, value\u003dj_username}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:215)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:167)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:671)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:410)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByName(RemoteWebDriver.java:485)\r\n\tat org.openqa.selenium.By$ByName.findElement(By.java:303)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:402)\r\n\tat br.com.zatix.LinkerPage.visita(LinkerPage.java:11)\r\n\tat br.com.zatix.CadastroUsuarioComumPapeisTest.acessoPortal(CadastroUsuarioComumPapeisTest.java:26)\r\n\tat ✽.Quando ele logar no sistema com o login correto(02_00_CadastroUsuarioComumPapeisTest.feature:10)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "Controle de Jornada",
      "offset": 45
    }
  ],
  "location": "CadastroUsuarioComumPapeisTest.testaCadastroUsuarioComumPapeis(String)"
});
formatter.result({
  "status": "skipped"
});
});