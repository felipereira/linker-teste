#language: pt

@RedefinicaoSenhaMotorista
Funcionalidade: Redefinir senha de um motorista no Omniweb

Cenario: Redefinir a senha de um motorista no portal Omniweb

Dado o cliente entrar no endereco web

Quando o usuario se conectar ao sistema Linker com usuario e senha corretos

Entao o usuario podera redefinir a senha do motorista com o nome "Pedro" entrando no menu lateral Motoristas/Consultar/redefinir