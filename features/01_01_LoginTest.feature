#language: pt

@Login
Funcionalidade:Logar no Portal Omniweb com credenciais validas

Cenario: Login no portal Omniweb com usuario valido

Dado que o usuario tenha entrado no Portal Omniweb

Quando ele informar o usuario e senha corretamente e clicar no botao de login

Entao ele deve logar no sistema