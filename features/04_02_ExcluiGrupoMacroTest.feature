#language: pt

@ExclusaoGrupoMacro
Funcionalidade: Exlusao de um  grupo de macros existente no portal Omniweb	

Cenario: Excluir um grupo de macros existentes no portal Omniweb

Dado o usuario abrir o seu navegador

Quando o mesmo acessar o portal com suas credencias

Entao o usuario podera excluir o grupo "AATEST" de macros ao entrar no menu lateral Macros/Gerenciar/Excluir