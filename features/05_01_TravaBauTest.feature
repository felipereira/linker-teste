#language: pt
# encoding: 'UTF-8'

@TravaBau
Funcionalidade: Comando Trava de Bau 

Cenario: Travar o bau de um rastreador instalado

Dado o cliente acessar o portal

Quando o cliente se logar no sistema

Entao o cliente podera selecionar o rastreador exemplo "750984" e enviar o comando "Travar Ba"