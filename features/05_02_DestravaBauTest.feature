#language: pt

@DestravaBau
Funcionalidade: Comando Destrava de Bau 

Cenario: Destravar o bau de um rastreador instalado

Dado o cliente acessar o portal Omniweb

Quando o cliente se logar no sistema Omniweb

Entao o usuario podera selecionar o rastreador exemplo "750984" e enviar o comando "Destravar Ba"