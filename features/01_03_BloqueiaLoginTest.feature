#language: pt

@BloqueiaLogin
Funcionalidade: Bloqueio de usuario apos quinze tentativas

Cenario: Bloquear um usuario apos quinze tentativas

Dado o usuario abrir o navegador e abrir o portal Omniweb

Entao o usuario tentar logar com credencias invalidas por 15x, seu login sera bloqueado por 30min