#language: pt

@ExclusaoDeCerca
Funcionalidade: Exlusao de uma cerca cadastrada no portal Omniweb	

Cenario: Excluir uma cerca existente no portal Omniweb

Dado o usuario abrir o navegador e entrar no portal Omniweb

Quando o usuario informar o usuario/senha corretamente e clicar no botao de login

Entao o usuario podera excluir a cerca "Omni123" cadastrada