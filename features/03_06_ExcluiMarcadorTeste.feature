#language: pt

@ExclusaoDeMarcador
Funcionalidade: Excluir um marcador do mapa portal Omniweb	

Cenario: Excluir um marcador no mapa portal Omniweb

Dado o usuario ter acessado o portal Linker Rastreamento

Quando o usuario se logar no portal Linker Rastreamento

Entao o usuario podera excluir o marcador de nome "aeroporto" ao clicar no botao marcador no mapa