#language: pt

@ExclusaoMotorista
Funcionalidade: Exclusao de um motorista no Omniweb

Cenario: Excluir um motorista no portal Omniweb com nome Tiao

Dado o cliente entrar no endereco no navegador

Quando o usuario se logar no sistema Linker com usuario e senha corretos

Entao o usuario podera excluir o motorista com o nome "Pedro" entrando no menu lateral Motoristas/Consultar/excluir