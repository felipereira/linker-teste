#language: pt 

@CadastroMotorista
Funcionalidade: Cadastro de um motorista no Omniweb

Cenario: Cadastrar um motorista no portal Omniweb com nome Tiao

Dado o cliente acessar o endereco no navegador

Quando o mesmo se logar no sistema Linker com o usuario e senha corretos

Entao o usuario podera criar o motorista com o nome "Joao" entrando no menu lateral Motoristas/Consultar/sinal de mais