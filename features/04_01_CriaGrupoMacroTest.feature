#language: pt

@NovoGrupoMacro
Funcionalidade: Criacao de um novo grupo de macros no portal Omniweb	

Cenario: Criar um novo grupo de macro no portal Omniweb

Dado o usuario acessar o sistema Omniweb

Quando o mesmo acessar o portal Omniweb

Entao o usuario podera criar o grupo "AATEST" de macros ao entrar no menu lateral Macros/Gerenciar/Novo Grupo