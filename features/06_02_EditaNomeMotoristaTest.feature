#language: pt 

@EdicaoNomeMotorista
Funcionalidade: Editar um motorista cadastrado no Omniweb

Cenario: Editar o nome Tiao do motorista no portal Omniweb

Dado o cliente acessar o endereco na web

Quando o mesmo se logar no sistema de rastreamento Omniweb com o usuario e senha corretos

Entao o usuario podera editar o motorista de nome "Joao" para o nome "Pedro" entrando no menu lateral Motoristas/Consultar/Editar