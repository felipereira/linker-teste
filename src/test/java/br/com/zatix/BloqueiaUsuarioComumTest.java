package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class BloqueiaUsuarioComumTest {
	private static WebDriver driver;

	@Dado("^o usuario abrir o navegador para acesso ao portal$")
	public void Ambiente() {

		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o usuario se logar$")
	public void acessoPortal() throws InterruptedException {
		
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera bloquear o usuario \"(.*)\" entrado no menu Usuarios/Consultar$")
	public boolean Bloqueio(String usuario) throws InterruptedException {
		
		return BloqueiaUsuarioComum.testaBloqueio(usuario, driver);	
	}
}
