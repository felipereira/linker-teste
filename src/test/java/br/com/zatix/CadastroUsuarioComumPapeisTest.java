package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CadastroUsuarioComumPapeisTest {

	private static WebDriver driver;

	@Dado("^o usuario entrar no endereco$")
	public void configAmbiente() {

		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^ele logar no sistema com o login correto$")
	public void acessoPortal() {

		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera criar um usuario com papel \"(.*)\" entrando no menu lateral Papeis/Incluir$")
	public void testaCadastroUsuarioComumPapeis(String papel) throws Exception {

		CadastroUsuarioComumPapeis.CadastraUsuarioComumPapeis(papel, driver);
	}
}
