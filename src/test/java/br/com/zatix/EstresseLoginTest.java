package br.com.zatix;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;

public class EstresseLoginTest {
	private static WebDriver driver;

	@Dado("^o usuario acessar o software Omniweb$")
	public void configAmbiente() {
		ConfigSetup.setup(driver);
		
		driver = new ChromeDriver();
	}

	@Entao("^o usuario ira logar cinquenta vezes$")
	public void testaStressLogin() throws InterruptedException {

		EstresseLogin.testaEstresseLogin(driver);

	}

}
