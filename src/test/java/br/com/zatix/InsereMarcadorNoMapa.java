package br.com.zatix;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InsereMarcadorNoMapa {

	public static void testaInserirMarcadorNoMapa(String marker, String icon_name, WebDriver driver)
			throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		Actions action = new Actions(driver);
		WebElement searchBox = driver.findElement(By.id("OpenLayers_Layer_Vector_85_svgRoot"));
		action.contextClick(searchBox).perform();

		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//a[contains(text(),'Novo marcador aqui')]")));
		driver.findElement(By.xpath("//a[contains(text(),'Novo marcador aqui')]")).click();

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
				"//div[contains(@eventproxy,'isc_AddLandmarkView')]/div/div/div/div/div/form/table/tbody/tr[2]/td[2]/input")));
		driver.findElement(By
				.xpath("//div[contains(@eventproxy,'isc_AddLandmarkView')]/div/div/div/div/div/form/table/tbody/tr[2]/td[2]/input"))
				.sendKeys(marker);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
				"//table[contains(@style,'cursor:default;width:196px;margin:0px;border:0px;padding:0px;background-image:none;background-color:transparent;-webkit-box-shadow:none;box-shadow:none;')]/tbody/tr/td/input")));
		driver.findElement(By
				.xpath("//table[contains(@style,'cursor:default;width:196px;margin:0px;border:0px;padding:0px;background-image:none;background-color:transparent;-webkit-box-shadow:none;box-shadow:none;')]/tbody/tr/td/input"))
				.clear();
		driver.findElement(By
				.xpath("//table[contains(@style,'cursor:default;width:196px;margin:0px;border:0px;padding:0px;background-image:none;background-color:transparent;-webkit-box-shadow:none;box-shadow:none;')]/tbody/tr/td/input"))
				.sendKeys(icon_name);

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'airplane')]")));
		driver.findElement(By.xpath("//div[contains(text(),'airplane')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//label[contains(text(),'Refer')]")));
		driver.findElement(By.xpath("//label[contains(text(),'Refer')]")).click();

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//td[contains(text(),'OK')]")));
		driver.findElement(By.xpath("//td[contains(text(),'OK')]")).click();

		try {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
					By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']")));
			driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']"))
					.click();

			assertTrue(mostraMarcadorNoMapa(driver, marker));

			driver.quit();

		} catch (Exception e) {
			System.out.println("Error: " + e);
		}

	}

	public static boolean mostraMarcadorNoMapa(WebDriver driver, String name_marker) {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//div[contains(@eventproxy,'isc_SimpleListGrid_0_body')]/div/table/tbody/tr/td/div")));
		driver.findElement(
				By.xpath("//div[contains(@eventproxy,'isc_SimpleListGrid_0_body')]/div/table/tbody/tr/td/div")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//div[contains(@eventproxy,'isc_ToolStrip_3')]/div/div[3]/img")));
		driver.findElement(By.xpath("//div[contains(@eventproxy,'isc_ToolStrip_3')]/div/div[3]/img")).click();

		return driver.getPageSource().contains(name_marker);
	}
}
