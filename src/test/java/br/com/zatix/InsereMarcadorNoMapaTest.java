
package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class InsereMarcadorNoMapaTest {

	private static WebDriver driver;

	@Dado("^acesso ao portal Linker Rastreamento$")
	public void configSetup() {
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^entrar no portal Omniweb$")
	public void acessoPortal() {
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^sera possivel inserir um marcador \"(.*)\" com o icone \"(.*)\" ao clicar na opcao novo marcador$")
	public void testaCriarMarcador(String nome_marcador, String marker) throws InterruptedException {
		InsereMarcadorNoMapa.testaInserirMarcadorNoMapa(nome_marcador, marker, driver);
	}
}
