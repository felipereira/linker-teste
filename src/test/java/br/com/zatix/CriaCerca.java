package br.com.zatix;

import static org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CriaCerca {

	private static WebDriverWait wait;

	public static void testaCriacaoDeCerca(String nome_cerca, WebDriver driver) throws InterruptedException {

		wait = new WebDriverWait(driver, 10);

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://200.185.141.80/Api_Js_v3/img_3/zoom-minus-mini.png']")));
		driver.findElement(By.cssSelector("img[src='http://200.185.141.80/Api_Js_v3/img_3/zoom-minus-mini.png']"))
				.click();
		driver.findElement(By.cssSelector("img[src='http://200.185.141.80/Api_Js_v3/img_3/zoom-minus-mini.png']"))
				.click();
		driver.findElement(By.cssSelector("img[src='http://200.185.141.80/Api_Js_v3/img_3/zoom-minus-mini.png']"))
				.click();
		driver.findElement(By.cssSelector("img[src='http://200.185.141.80/Api_Js_v3/img_3/zoom-minus-mini.png']"))
				.click();

		Thread.sleep(3000);
		Actions action = new Actions(driver);
		WebElement searchBox = driver.findElement(By.id("OpenLayers_Layer_Vector_85_svgRoot"));
		action.contextClick(searchBox).perform();

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//a[contains(text(),'Cercas')]")));
		driver.findElement(By.xpath("//a[contains(text(),'Cercas')]")).click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//a[contains(text(), 'Criar cerca circular')]")));
		driver.findElement(By.xpath("//a[contains(text(), 'Criar cerca circular')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[text()='OK']")));
		driver.findElement(By.xpath("//div[text()='OK']")).click();

		WebElement click = driver.findElement(By.id("OpenLayers_Layer_Vector_85_svgRoot"));
		action.click(click).perform();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("name")));
		driver.findElement(By.name("name")).sendKeys(nome_cerca);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("group")));
		driver.findElement(By.name("group")).clear();
		driver.findElement(By.name("group")).sendKeys("Teste");

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'teste')]")));
		driver.findElement(By.xpath("//div[contains(text(),'teste')]")).click();

		try {

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
					By.xpath("//*[contains(@eventproxy,'isc_DynamicForm_8')]/div/div/div")));
			driver.findElement(By.xpath("//*[contains(@eventproxy,'isc_DynamicForm_8')]/div/div/div")).click();

		} catch (Exception e) {

			System.out.println("Error: " + e);

		} finally {

			driver.findElement(By.tagName("body")).sendKeys(Keys.TAB, Keys.TAB, Keys.ENTER);
			driver.findElement(By.name("entryEventType")).clear();
			driver.findElement(By.name("entryEventType")).sendKeys("Alarme", Keys.ENTER);
			driver.findElement(By.tagName("body")).sendKeys(Keys.TAB, "Bloquear Veículo", Keys.ENTER);

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//td[text()='OK']")));
			driver.findElement(By.xpath("//td[text()='OK']")).click();

			assertTrue(validaCercaCriada(nome_cerca, driver));

			driver.quit();
		}
	}

	public static boolean validaCercaCriada(String nome_cerca, WebDriver driver) {

		wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("Cerca")));
		driver.findElement(By.id("Cerca")).click();

		return driver.getPageSource().contains(nome_cerca);
	}
}
