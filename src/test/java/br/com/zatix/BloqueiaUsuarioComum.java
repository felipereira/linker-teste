package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BloqueiaUsuarioComum {
	public static boolean testaBloqueio(String usuario, WebDriver driver) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));

		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")).click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//table//tbody//tr//td//div[contains(text(),'Usu')]")));
		driver.findElement(By.xpath("//table//tbody//tr//td//div[contains(text(),'Usu')]")).click();

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Consultar')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Consultar')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("usernameText")));
		driver.findElement(By.name("usernameText")).sendKeys(usuario);

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Pesquisar')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Pesquisar')]")).click();

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/locked.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/locked.png']")).click();

		boolean flag = true;

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/locked_red.png']")));

		boolean isNEmpty = driver
				.findElements(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/locked_red.png']"))
				.size() > 0;

		if (!isNEmpty)
			flag = false;

		driver.quit();

		return flag;

	}

}
