package br.com.zatix;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CriaCategoria {

	public static void testaCriarMarcador(WebDriver driver, String nome_marcador) {

		WebDriverWait wait = new WebDriverWait(driver, 15);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']"))
				.click();

		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//table/tbody/tr/td[contains(text(),'Categoria')]")));
		driver.findElement(By.xpath("//table/tbody/tr/td[contains(text(),'Categoria')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/addLine.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/addLine.png']")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//div[contains(@style,'overflow:hidden;text-overflow:ellipsis;WIDTH:42px;')]")));
		driver.findElement(By.xpath("//div[contains(@style,'overflow:hidden;text-overflow:ellipsis;WIDTH:42px;')]"))
				.click();

		try {
			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
					"//table/tbody/tr/td/div/span[contains(@style,'http://10.40.1.58:7110/portal/img/icon/category/Aerop01.png')]")));
			driver.findElement(By
					.xpath("//table/tbody/tr/td/div/span[contains(@style,'http://10.40.1.58:7110/portal/img/icon/category/Aerop01.png')]"))
					.click();
		} catch (Exception e) {
			System.out.println("Error: e" + e);
		}

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
				"//div[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[4]/div/span/input")));
		driver.findElement(By
				.xpath("//div[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[4]/div/span/input"))
				.sendKeys(nome_marcador, Keys.ENTER);

		try {
			WebElement contains = driver.findElement(
					By.xpath("//div[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[4]/div"));

			String text = contains.getText();

			String validate = text.replaceAll("[^a-z]", "");
			String validate1 = nome_marcador.replaceAll("[^a-z]", "");

			assertTrue(validate.equals(validate1));

		} catch (Exception e) {
			System.out.println("Error: " + e);
		}
		driver.quit();
	}
}
