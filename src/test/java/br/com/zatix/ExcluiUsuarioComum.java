package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExcluiUsuarioComum {
	
	public static void testaExcluirUsuarioComum(String usuario, WebDriver driver) throws InterruptedException{
		
	WebDriverWait wait = new WebDriverWait(driver, 10);
		
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")).click();

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//table//tbody//tr//td//div[contains(text(),'Usu')]")));
		driver.findElement(By.xpath("//table//tbody//tr//td//div[contains(text(),'Usu')]")).click();

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Consultar')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Consultar')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("usernameText")));
		driver.findElement(By.name("usernameText")).sendKeys(usuario);

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Pesquisar')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Pesquisar')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/delete.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/delete.png']")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Sim')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Sim')]")).click();

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'OK')]")));
		driver.findElement(By.xpath("//div[contains(text(),'OK')]")).click();

		driver.quit();
	}

}
