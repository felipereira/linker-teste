package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginAcesso {
	
	private WebDriver driver;
	
	public LoginAcesso(WebDriver driver){
		this.driver = driver;
	}
	
	public LoginAcesso visita(String url){
		driver.get(url);
		return this;
	}
	
	public LoginValida autentica(String usuario, String senha){
		
		driver.findElement(By.name("j_username")).sendKeys(usuario);
		driver.findElement(By.name("j_password")).sendKeys(senha);
		driver.findElement(By.className("loginButton")).click();
		
		return new LoginValida(driver);
	}

}
