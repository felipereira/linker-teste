package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class EditaNomeMotorista {

	public static void testaEditarNomeMotorista(String nomeMotorista, String alteraApelido, WebDriver driver)
			throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		boolean validate;

		try {
			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
					By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
			driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']"))
					.click();

			Thread.sleep(2000);
			wait.until(ExpectedConditions
					.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Motoristas')]")));
			driver.findElement(By.xpath("//td//div[contains(text(),'Motoristas')]")).click();

			wait.until(ExpectedConditions
					.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Consultar')]")));
			driver.findElement(By.xpath("//div[contains(text(),'Consultar')]")).click();

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("name$148l")));
			driver.findElement(By.name("name$148l")).sendKeys(nomeMotorista);
			
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
					By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/edit.png']")));
			driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/edit.png']")).click();

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("name")));
			driver.findElement(By.name("name")).clear();

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("name")));
			driver.findElement(By.name("name")).sendKeys(alteraApelido, Keys.ENTER);

			validate = driver.getPageSource().contains(alteraApelido);
			assertTrue(validate);
			
			driver.quit();
			
		} catch (Exception e) {
			System.out.println("Error");
		}

	}

}
