package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class EnviaMensagemTest {
	
	private static WebDriver driver;
	
	@Dado("^acessado o portal$")
	public void configSetup(){
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}
	@Quando("^logar no Omniweb$")
	public void acessaPortal(){
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}
	@Entao("^ao pesquisar o rastreador \"(.*)\" podera enviar mensagem clicando na figura de envelope$")
	public void testaEnviarMsg(String numero_serie){
		EnviaMensagem.testaEnviarMsg(numero_serie, driver);
	}

}
