package br.com.zatix;

import org.openqa.selenium.WebDriver;

public class LoginValida {

	private WebDriver driver;

	public LoginValida(WebDriver driver) {
		this.driver = driver;
	}

	public boolean isValida() {
		return validaURL();
	}

	private boolean validaURL() {
		return driver.getCurrentUrl().endsWith("#main") != false;
	}

}
