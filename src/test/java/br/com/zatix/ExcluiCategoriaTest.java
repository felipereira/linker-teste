package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class ExcluiCategoriaTest {
	
	private static WebDriver driver;
	
	@Dado("^o acesso ao portal Omniweb rastreamento de veiculos$")
	public void ConfigSetup(){
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}
	
	@Quando("^se logar no sistema  Omniweb rastreamento de veiculos$")
	public void acessoPortal(){
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}
	
	@Entao("^o usuario podera excluir a categoria \"(.*)\" clicando no botao marcadores/categoria$")
	public void testaExcluirCategoria(String nome_categoria){
		ExcluiCategoria.testaExcluirCategoria(driver, nome_categoria);
	}

}
