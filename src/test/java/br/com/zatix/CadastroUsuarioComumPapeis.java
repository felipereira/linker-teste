package br.com.zatix;

import java.util.ArrayList;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CadastroUsuarioComumPapeis {

	public static void CadastraUsuarioComumPapeis(String papel, WebDriver driver) throws Exception {

		String letras = "ABCDEFGHIJKLMNOPQRSTUVYWXZ", nome_usuario = "";
		int index = -1;

		WebDriverWait wait = new WebDriverWait(driver, 10);

		Thread.sleep(1000);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")).click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//table//tbody//tr//td//div[contains(text(),'Usu')]")));
		driver.findElement(By.xpath("//table//tbody//tr//td//div[contains(text(),'Usu')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Incluir')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Incluir')]")).click();

		ArrayList<String> list = new ArrayList<String>();

		list.add("Ativação de Rastreador");
		list.add("Compartilhamento de Sinal");
		list.add("Configuração Estendida do Rastreador");
		// list.add("Configuração Intervalo Poisição Satelital");
		list.add("Controle de Jornada");
		list.add("Controle de Telemetria");
		list.add("Gestão de Cercas Web Interna");
		list.add("Gestão de Cercas Web");
		list.add("Gestão de Frota");
		list.add("Gestão de Macros");
		list.add("Gestão de Marcadores Interna");
		list.add("Gestão de Marcadores");
		list.add("Gestão de Motorista");
		list.add("Gestão de Rotas Web");
		list.add("Gestão de Usuários Avançada");
		list.add("Gestão de Usuários Básica");
		list.add("Gestão de Usuários Interna");
		list.add("Gestão de Veículos Básica");
		// list.add("Rastreamento Básico Sat20");
		list.add("Rastreamento Básico");
		list.add("Rastreamento de Cercas Web");
		list.add("Rastreamento de Rotas Web");
		list.add("Serviço de Auxílio na Localização do Veículo");
		list.add("Suporte a Acelerômetro");
		list.add("Suporte a Bloqueio Remoto");
		list.add("Suporte a Carreta");
		list.add("Suporte a Comandos e Eventos de Segurança");
		list.add("Suporte a Comunicação Satélite Iridium");
		list.add("Suporte a Configuração de Satélite Ilimitada");
		list.add("Suporte a Lacre de Motor");
		list.add("Suporte a Replay de Percurso");
		list.add("Suporte a Sensor de 5a Roda");
		list.add("Suporte a Sensor de Baú");
		list.add("Suporte a Sensor de Engate de Carreta");
		list.add("Suporte a Sensores Configuráveis");
		list.add("Suporte a Sensores de Painel");
		list.add("Suporte a Sensores de Portas");
		list.add("Suporte a Sirene");
		list.add("Suporte a Temperatura");
		list.add("Suporte a Terminal");
		list.add("Suporte a Trava de Baú");
		list.add("Suporte ao Rastreador");
		list.add("Suporte aos Usuários");
		list.add("Tratamento de Pânico");

		for (int i = 0; i < list.size(); i++) {

			boolean test = driver.getPageSource().contains(list.get(i));

			try {

				if (test == false) {
					wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
							By.xpath("//td[contains(text(),'" + list.get(i) + "')]")));
				}

			} catch (Exception e) {

				System.out.println("Error: " + e);

				Screenshot print = new Screenshot();
				print.capturaimagem(driver, papel);
			}
		}

		Random random = new Random();

		for (int i = 0; i < 9; i++) {
			index = random.nextInt(letras.length());
			nome_usuario += letras.substring(index, index + 1);
		}

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("userText")));
		driver.findElement(By.name("userText")).sendKeys(nome_usuario);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("firstNameText")));
		driver.findElement(By.name("firstNameText")).sendKeys("Felipe");

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("lastNameText")));
		driver.findElement(By.name("lastNameText")).sendKeys("Pereira");

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("emailText")));
		driver.findElement(By.name("emailText")).sendKeys(nome_usuario + "@omnilink.com.br");

		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//td[contains(text(),'" + papel + "')]")));

		driver.findElement(By.xpath("//td[contains(text(),'" + papel + "')]")).click();

		driver.findElement(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/skins/Linker/images/TransferIcons/right.png']"))
				.click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//td[contains(text(),'OK')]")));
		driver.findElement(By.xpath("//td[contains(text(),'OK')]")).click();

		driver.quit();
	}

}
