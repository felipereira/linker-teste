package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class ExcluiCercaTest {
	WebDriver driver;

	@Dado("^o usuario abrir o navegador e entrar no portal Omniweb$")
	public void abrirBrowser() {

		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o usuario informar o usuario/senha corretamente e clicar no botao de login$")
	public void acessoPortal() throws InterruptedException {

		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera excluir a cerca \"(.*)\" cadastrada$")
	public void TestaExcluirCerca(String cerca) throws InterruptedException {

		ExcluiCerca.testaExcluirCerca(cerca, driver);

	}
}