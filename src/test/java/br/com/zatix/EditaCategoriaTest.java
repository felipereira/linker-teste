package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class EditaCategoriaTest {

	private static WebDriver driver;

	@Dado("^acessado portal Omniweb Rastreamento$")
	public void ConfigSetup() {
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}
	@Quando("^logado no portal Omniweb Rastreamento$")
	public void acessoPortal() {
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}
	@Entao("^sera possivel editar uma categoria com o nome \"(.*)\" ao clicar no botao marcado no mapa$")
	public void testaEditarMarcadorTest(String new_marker_name) {
		EditaCategoria.testaEdicaoDeMarcador(driver, new_marker_name);

	}
}
