package br.com.zatix;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class DestravaBauTest {

	WebDriver driver;

	@Dado("^o cliente acessar o portal Omniweb$")
	public void configAmbiente() {
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o cliente se logar no sistema Omniweb$")
	public void acessoPortal() {
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera selecionar o rastreador exemplo \"(.*)\" e enviar o comando \"(.*)\"$")
	public void testaDestravarBau(String id, String destravarBau) throws InterruptedException {

		DestravaBau.testaDestravarBau(id, destravarBau, driver);
	}
}
