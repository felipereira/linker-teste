package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExcluiCerca {

	private static WebDriverWait wait;

	public static void testaExcluirCerca(String cerca, WebDriver driver) throws InterruptedException {

		wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("Cerca")));
		driver.findElement(By.id("Cerca")).click();
		driver.findElement(By.tagName("body")).sendKeys(Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB,
				cerca);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div [aria-label='Excluir']")));
		driver.findElement(By.cssSelector("div [aria-label='Excluir']")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[text()='Sim']")));
		driver.findElement(By.xpath("//div[text()='Sim']")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//div[contains(@eventproxy,'isc_Portlet_2_header')]/div[3]/img")));
		driver.findElement(
				By.xpath("//div[contains(@eventproxy,'isc_Portlet_2_header')]/div[3]/img"))
				.click();

		assertTrue(verificaCerca(cerca, driver));

		driver.quit();
	}

	public static boolean verificaCerca(String nome_cerca, WebDriver driver) {

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("Cerca")));
		driver.findElement(By.id("Cerca")).click();

		return driver.getPageSource().contains(nome_cerca) != true;
	}
}
