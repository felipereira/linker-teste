package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.assertEquals;

public class ExcluiMotorista {

	public static void testaExcluirMotorista(WebDriver driver, String nomeMotorista) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		boolean validate, noContains = false;

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")).click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Motoristas')]")));
		driver.findElement(By.xpath("//td//div[contains(text(),'Motoristas')]")).click();

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Consultar')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Consultar')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("name$148l")));
		driver.findElement(By.name("name$148l")).sendKeys(nomeMotorista);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/print.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/delete.png']")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Sim')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Sim')]")).click();
		
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("name$148l")));
		driver.findElement(By.name("name$148l")).clear();

		validate = driver.getPageSource().contains(nomeMotorista);
	
		assertEquals(validate, noContains);

		driver.quit();
	}
}
