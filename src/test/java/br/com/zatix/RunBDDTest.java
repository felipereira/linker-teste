package br.com.zatix;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(strict = false, features = "features", format = { "pretty", "html:target/site/cucumber-pretty",
		"json:target/cucumber.json" }, tags = {"@CadastroUsuarioPapel"})
public class RunBDDTest {

}	