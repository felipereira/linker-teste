package br.com.zatix;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class VisualizaUsuarioComumTest {

	private static WebDriver driver;

	@Dado("^o usuario abrir o Navegador$")
	public void abrirBrowser() {

		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o usuario entrar no sistema Omniweb$")
	public void acessoPortal() throws InterruptedException {

		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());

	}

	@Entao("^o usuario podera visualizar o \"(.*)\" entrado no menu Usuarios/ Consultar$")
	public void TestaVisualizarUsuarioComum(String usuario) throws InterruptedException {

		VisualizaUsuarioComum.testaVisualizarUsuarioComum(usuario, driver);
	}

}
