package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CadastroUsuarioComum {

	public static void testaCadastroUsuarioComum(String nome, WebDriver driver) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")).click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//table//tbody//tr//td//div[contains(text(),'Usu')]")));
		driver.findElement(By.xpath("//table//tbody//tr//td//div[contains(text(),'Usu')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Incluir')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Incluir')]")).click();

		try {

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("userText")));
			driver.findElement(By.name("userText")).sendKeys(nome);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("firstNameText")));
			driver.findElement(By.name("firstNameText")).sendKeys("Felipe");
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("lastNameText")));
			driver.findElement(By.name("lastNameText")).sendKeys("Pereira");
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("emailText")));
			driver.findElement(By.name("emailText")).sendKeys(nome + "@hotmail.com");

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(
					"img[src='http://10.40.1.58:7110/portal/skins/Linker/images/TransferIcons/right_all.png']")));
			driver.findElement(By.cssSelector(
					"img[src='http://10.40.1.58:7110/portal/skins/Linker/images/TransferIcons/right_all.png']"))
					.click();

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//td[contains(text(),'OK')]")));
			driver.findElement(By.xpath("//td[contains(text(),'OK')]")).click();

			// assertTrue(driver.getPageSource().contains("FelipeTest"));

		} catch (Exception e) {

			Screenshot print = new Screenshot();
			print.capturaimagem(driver, nome);

		} finally {

			boolean isNEmpty = driver.findElements(By.xpath("//td[contains(text(),\"obrigat\")]")).size() > 0;

			if (isNEmpty) {

				Thread.sleep(1000);
				driver.findElement(By.xpath("//div[contains(text(),'OK')]")).click();
			}

			try {

				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
						By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));

			} catch (UnhandledAlertException f) {

				f.printStackTrace();

			}
		}
		driver.quit();
	}
}
