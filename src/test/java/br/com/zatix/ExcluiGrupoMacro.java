package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExcluiGrupoMacro {

	public static void TestaExcluirGrupoMacro(String nomeMacro, WebDriver driver) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")).click();
		
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div [aria-label='Macros']")));
		driver.findElement(By.cssSelector("div [aria-label='Macros']")).click();

		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Gerenciar')]")));
		driver.findElement(By.xpath("//td//div[contains(text(),'Gerenciar')]")).click();

		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.cssSelector("#isc_MacroManagementView_2_0_valueCell0")));

		WebElement botao = driver.findElement(By.cssSelector("#isc_MacroManagementView_2_0_valueCell0"));

		String validate = botao.getText();

		if (validate.equals(nomeMacro)) {

			try {

				Actions action = new Actions(driver).contextClick(botao);
				action.build().perform();

				wait.until(ExpectedConditions
						.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Excluir')]")));
				driver.findElement(By.xpath("//div[contains(text(),'Excluir')]")).click();

				wait.until(ExpectedConditions
						.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Sim')]")));
				driver.findElement(By.xpath("//td//div[contains(text(),'Sim')]")).click();

				Thread.sleep(1000);

			} catch (Exception e) {
				System.out.println("Error: " + e);
			}
		} else {
			System.out.println("Error \n Verifique o codigo acima");
		}

		driver.quit();
	}

}
