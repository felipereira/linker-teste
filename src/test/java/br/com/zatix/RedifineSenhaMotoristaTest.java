package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class RedifineSenhaMotoristaTest {
	
	private static WebDriver driver;
	
	@Dado("^o cliente entrar no endereco web$")
	public void ConfigSetup(){
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o usuario se conectar ao sistema Linker com usuario e senha corretos$")
	public void acessoPortal(){
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}
	
	@Entao("^o usuario podera redefinir a senha do motorista com o nome \"(.*)\" entrando no menu lateral Motoristas/Consultar/redefinir$")
	public void testaRedefinirSenhaMotorista(String nomeMotorista) throws InterruptedException{
		RedefineSenhaMotorista.testeRedefinirSenhaMotorista(nomeMotorista, driver);
	}
}
