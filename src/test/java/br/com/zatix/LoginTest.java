package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class LoginTest {
	
	WebDriver driver = null;

	@Dado("^que o usuario tenha entrado no Portal Omniweb$")
	public void abrirBrowser() {
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^ele informar o usuario e senha corretamente e clicar no botao de login$")
	public void acessarPortal() throws InterruptedException {
		
		LoginAcesso loginPage = new LoginAcesso(driver);
		LoginValida homePage = loginPage.visita("http://10.40.1.58:7110/portal/Login.html")
				.autentica("02884318000108","123456");

		assertTrue(homePage.isValida());
	}

	@Entao("^ele deve logar no sistema$")
	public void fecharBrowser() {
		driver.close();
	}
}
