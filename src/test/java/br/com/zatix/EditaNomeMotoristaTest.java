package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class EditaNomeMotoristaTest {
	
	private static WebDriver driver;
	
	@Dado("^o cliente acessar o endereco na web$")
	public void configSetup(){
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}
	@Quando("^o mesmo se logar no sistema de rastreamento Omniweb com o usuario e senha corretos$")
	public void acessoPortal(){
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}
	@Entao("^o usuario podera editar o motorista de nome \"(.*)\" para o nome \"(.*)\" entrando no menu lateral Motoristas/Consultar/Editar$")
	public void testaEditarNomeMotorista(String nomeMotorista, String alteraApelido) throws InterruptedException{
		EditaNomeMotorista.testaEditarNomeMotorista(nomeMotorista, alteraApelido, driver);
	}
	
}
