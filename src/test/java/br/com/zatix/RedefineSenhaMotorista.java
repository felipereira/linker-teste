package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class RedefineSenhaMotorista {

	public static void testeRedefinirSenhaMotorista(String nomeMotorista, WebDriver driver)
			throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		WebElement password, new_password;

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")).click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Motoristas')]")));
		driver.findElement(By.xpath("//td//div[contains(text(),'Motoristas')]")).click();

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Consultar')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Consultar')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("name$148l")));
		driver.findElement(By.name("name$148l")).sendKeys(nomeMotorista);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//*[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[3]/div")));

		password = driver.findElement(
				By.xpath("//*[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[3]/div"));
		
		String passwordOld = password.getText();
		int password_old = Integer.parseInt(passwordOld);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/changePassword.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/changePassword.png']"))
				.click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/skins/Linker/images/headerIcons/close.png']")));
		driver.findElement(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/skins/Linker/images/headerIcons/close.png']"))
				.click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//*[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[3]/div")));

		new_password = driver.findElement(
				By.xpath("//*[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[3]/div"));
		
		String passwordNew = new_password.getText();
		int password_new = Integer.parseInt(passwordNew);

		assertTrue(validaSenha(password_old, password_new));

		driver.quit();
	}

	public static boolean validaSenha(int passwordOld, int passwordNew) {

		return passwordOld != passwordNew;
	}

}
