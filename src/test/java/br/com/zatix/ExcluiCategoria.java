package br.com.zatix;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExcluiCategoria {

	public static void testaExcluirCategoria(WebDriver driver, String nome_categoria) {

		WebDriverWait wait = new WebDriverWait(driver, 15);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']"))
				.click();

		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//table/tbody/tr/td[contains(text(),'Categoria')]")));
		driver.findElement(By.xpath("//table/tbody/tr/td[contains(text(),'Categoria')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
				"//div[contains(@eventproxy,'isc_BaseListGrid_1filterEditor_body')]/form/div/table/tbody/tr/td[4]/div/span/input")));
		driver.findElement(By
				.xpath("//div[contains(@eventproxy,'isc_BaseListGrid_1filterEditor_body')]/form/div/table/tbody/tr/td[4]/div/span/input"))
				.sendKeys(nome_categoria, Keys.ENTER);

		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(@aria-label, 'Excluir')]/img")));
		driver.findElement(By.xpath("//div[contains(@aria-label, 'Excluir')]/img")).click();
		
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(), 'Sim')]")));
		driver.findElement(By.xpath("//div[contains(text(), 'Sim')]")).click();
		
		try{
			Thread.sleep(2000);
			assertTrue(validaCategoria(nome_categoria, driver));
			
		}catch(Exception e){
			System.out.println("Error: " + e);
		}
		
		driver.quit();
	}
	
	public static boolean validaCategoria(String nome_categoria, WebDriver driver){
		
		return driver.getPageSource().contains(nome_categoria) != true;
	}
}
