package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CadastroMotoristaTest {

	private static WebDriver driver;

	@Dado("^o cliente acessar o endereco no navegador$")
	public void confAmbiente() {
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o mesmo se logar no sistema Linker com o usuario e senha corretos$")
	public void acessoPortal() {
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera criar o motorista com o nome \"(.*)\" entrando no menu lateral Motoristas/Consultar/sinal de mais$")
	public void testaCadastroMotoristaTest(String nomeMotorista) throws InterruptedException {
		CadastroMotorista.testaCadastraMotorista(driver, nomeMotorista);
	}

}
