package br.com.zatix;

import static org.junit.Assert.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class ExcluiGrupoMacroTest {

	public static void main(String[] args) {
	}

	WebDriver driver;

	@Dado("^o usuario abrir o seu navegador$")
	public void configAmbiente() {

		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o mesmo acessar o portal com suas credencias$")
	public void acessoPortal() throws InterruptedException {

		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera excluir o grupo \"(.*)\" de macros ao entrar no menu lateral Macros/Gerenciar/Excluir$")
	public void TestaExcluirGrupoMacro(String nomeMacro) throws InterruptedException {
		ExcluiGrupoMacro.TestaExcluirGrupoMacro(nomeMacro, driver);
	}
}