package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CriaCategoriaTest {
	
	private static WebDriver driver;
	
	@Dado("^o cliente acessar o portal Omniweb Rastreamento$")
	public void configSetup(){
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}
	
	@Quando("^o usuario se logar no portal Omniweb Rastreamento$")
	public void acessoPortal(){
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}
	
	@Entao("^O usuario podera criar uma categoria com o nome \"(.*)\" ao clicar no botao marcado no mapa$")
	public void testaCriarMarcador(String nome_marcador){
		CriaCategoria.testaCriarMarcador(driver, nome_marcador);
	}
}
