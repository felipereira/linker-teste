package br.com.zatix;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import cucumber.api.java.pt.Dados;
import cucumber.api.java.pt.Entao;

public class BloqueiaLoginTest {
	static WebDriver driver;

	@Dados("^o usuario abrir o navegador e abrir o portal Omniweb$")
	public void configAmbiente() {

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true);
		caps.setCapability("takesScreenshot", true);
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
				"C:\\Users\\felipe.pereira\\workspace\\selenium-java-3.0.1\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");

		driver = new PhantomJSDriver(caps);
	}

	@Entao("^o usuario tentar logar com credencias invalidas por 15x, seu login sera bloqueado por 30min$")
	public void TestaLogin() throws InterruptedException {

		BloqueioLogin.testaBloqueioLogin(driver);
	}

}
