package br.com.zatix;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import static org.junit.Assert.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CadastroMotorista {

	public static void testaCadastraMotorista(WebDriver driver, String nameMotor) throws InterruptedException {

		String name = null, alpha = "ABCDEFGHIJKLMNOPQRSTUVYWXZ", date;
		int i, index = -1;
		boolean validate;

		WebDriverWait wait = new WebDriverWait(driver, 10);

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")).click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Motoristas')]")));
		driver.findElement(By.xpath("//td//div[contains(text(),'Motoristas')]")).click();

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Consultar')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Consultar')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/addLine.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/addLine.png']")).click();

		Random random = new Random();

		for (i = 0; i < 9; i++) {
			index = random.nextInt(alpha.length());
			name += alpha.substring(index, index + 1);
		}

		Date today = new Date();

		SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yy");
		date = date_format.format(today);

		try {

			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("nickname")));
			driver.findElement(By.name("nickname")).sendKeys(name);

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("name")));
			driver.findElement(By.name("name")).sendKeys(nameMotor);

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("license")));
			driver.findElement(By.name("license")).sendKeys("11111111111");

			wait.until(ExpectedConditions
					.visibilityOfAllElementsLocatedBy(By.name("licenseExpirationDate_dateTextField")));
			driver.findElement(By.name("licenseExpirationDate_dateTextField")).sendKeys(date);

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("licenseCategory")));
			driver.findElement(By.name("licenseCategory")).sendKeys("A", Keys.ENTER, Keys.ESCAPE);

			validate = driver.getPageSource().contains(name);

			assertTrue(validate);

			driver.quit();

		} catch (Exception e) {

			System.out.printf("Error", e);

		}
	}
}
