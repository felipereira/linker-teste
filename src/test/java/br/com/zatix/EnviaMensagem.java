package br.com.zatix;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EnviaMensagem {

	public static void testaEnviarMsg(String numero_serie, WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("licensePlate$148l")));
		driver.findElement(By.name("licensePlate$148l")).sendKeys(numero_serie);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/readMessage.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/readMessage.png']"))
				.click();

		try {
			
			Date today = new Date();
			SimpleDateFormat date_format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
			String date = date_format.format(today);
			
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("messageTextArea")));
			driver.findElement(By.className("messageTextArea")).sendKeys("Hello World " + date);
			
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Enviar')]")));
			driver.findElement(By.xpath("//div[contains(text(),'Enviar')]")).click();

			driver.quit();

		} catch (Exception e) {
			System.out.println("Error: " + e);
		}

	}

}
