package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class ExcluiMarcador {

	public static void testaExcluirMarcador(String name_marker, WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']"))
				.click();

		WebElement element = driver.findElement(
				By.xpath("//div[contains(@eventproxy,'isc_SimpleListGrid_0_body')]/div/table/tbody/tr/td[5]/div"));
		String name_element = element.getText().replaceAll("[^a-z]", "");

		try {
			if (name_element.equals(name_marker.replaceAll("[^a-z]", ""))) {

				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
						By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/delete.png']")));
				driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/delete.png']"))
						.click();
				
				Thread.sleep(2000);
				wait.until(
						ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Sim')]")));
				driver.findElement(By.xpath("//div[contains(text(),'Sim')]")).click();

				assertFalse(verificaMarcador(name_marker, driver));

				driver.quit();

			} else {
				System.out.println("Nome do marcador esta diferente");
			}
		} catch (Exception e) {
			System.out.println("Error: " + e);
		}
	}

	public static boolean verificaMarcador(String marker, WebDriver driver) throws InterruptedException {
		
		Thread.sleep(2000);
		return driver.getPageSource().contains(marker);
	}

}
