package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CriaGrupoMacroTest {

	private static WebDriver driver;

	@Dado("^o usuario acessar o sistema Omniweb$")
	public void configAmbiente() {

		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o mesmo acessar o portal Omniweb$")
	public void abrirPortal() throws InterruptedException {

		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera criar o grupo \"(.*)\" de macros ao entrar no menu lateral Macros/Gerenciar/Novo Grupo$")
	public void TestaCriacaoGrupoMacro(String grupoMacro) throws InterruptedException {

		CriaGrupoMacro.testaCriacaoGrupoMacro(grupoMacro, driver);
	}
}
