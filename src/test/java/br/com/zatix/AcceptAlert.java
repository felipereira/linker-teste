package br.com.zatix;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

public class AcceptAlert {

	public static void trataAlertLinker(WebDriver driver) throws InterruptedException {

		Thread.sleep(10000);
		driver.switchTo().alert().accept();
	}

	public static void trataAlertLinkerGhost(WebDriver driver) throws InterruptedException {

		if (driver instanceof PhantomJSDriver) {
			PhantomJSDriver phantom = (PhantomJSDriver) driver;
			phantom.executeScript("window.alert = function(){}");
			phantom.executeScript("window.confirm = function(){return true;}");
		} else
			driver.switchTo().alert().accept();
	}

}
