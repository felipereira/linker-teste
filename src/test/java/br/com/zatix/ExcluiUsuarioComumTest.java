package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class ExcluiUsuarioComumTest {

	WebDriver driver;

	@Dado("^o usuario abrir o navegador para acessar o portal$")
	public void configAmbiente() {

		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o usuario se logar com credencias validas$")
	public void acessaPortal() throws InterruptedException {

		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera exluir o usuario \"(.*)\" entrado no menu Usuarios/Consultar$")
	public void TestaExcluirUsuario(String usuario) throws InterruptedException {

		ExcluiUsuarioComum.testaExcluirUsuarioComum(usuario, driver);
	}
}
