package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CriaGrupoMacro {

	public static void testaCriacaoGrupoMacro(String nomeGrupo, WebDriver driver) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("isc_4N")));
		driver.findElement(By.id("isc_4N")).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div [aria-label='Macros']")));
		driver.findElement(By.cssSelector("div [aria-label='Macros']")).click();
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Gerenciar')]")));
		driver.findElement(By.xpath("//td//div[contains(text(),'Gerenciar')]")).click();
		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div [aria-label='Novo Grupo']")));
		driver.findElement(By.cssSelector("div [aria-label='Novo Grupo']")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("textItem")));
		driver.findElement(By.className("textItem")).clear();
		driver.findElement(By.className("textItem")).sendKeys(nomeGrupo);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("vehicleStateText")));
		driver.findElement(By.name("vehicleStateText")).clear();
		driver.findElement(By.name("vehicleStateText")).sendKeys("Rastreado");

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//tr//td[contains(text(),'Salvar')]")));
		driver.findElement(By.xpath("//tr//td[contains(text(),'Salvar')]")).click();

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//table//tbody//tr//td[2]//label[contains(text(),'Dispon')]")));
		driver.findElement(By.xpath("//table//tbody//tr//td[2]//label[contains(text(),'Dispon')]")).click();
		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//tr//td[contains(text(),'Salvar')]")));
		driver.findElement(By.xpath("//tr//td[contains(text(),'Salvar')]")).click();

		try {
			
			boolean achouGrupoMacro = driver.getPageSource().contains(nomeGrupo);
			assertTrue(achouGrupoMacro);

			driver.quit();

		} catch (Exception e) {
			System.out.println("Error " + e);
		}
	}

}
