package br.com.zatix;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RelatorioPermanenciaEmLocais {

	public static void testaGerarRelatorio(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, 15);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")).click();

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Frotas')]")));
		driver.findElement(By.xpath("//td//div[contains(text(),'Frotas')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Locais')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Locais')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//table/tbody/tr/td/input[contains(@name,'isc_VehicleUserComboBox')]")));
		driver.findElement(By.xpath("//table/tbody/tr/td/input[contains(@name,'isc_VehicleUserComboBox')]")).clear();
		driver.findElement(By.xpath("//table/tbody/tr/td/input[contains(@name,'isc_VehicleUserComboBox')]"))
				.sendKeys("Todos)");

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//table/tbody/tr/td/input[contains(@name,'isc_FleetComboBox')]")));
		driver.findElement(By.xpath("//table/tbody/tr/td/input[contains(@name,'isc_FleetComboBox')]")).clear();
		driver.findElement(By.xpath("//table/tbody/tr/td/input[contains(@name,'isc_FleetComboBox')]")).sendKeys("Frota",
				Keys.ENTER);

		try {

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			GregorianCalendar gc = new GregorianCalendar();
			gc.add(Calendar.DAY_OF_MONTH, -30);

			Date date = gc.getTime();
			String lastDate = sdf.format(date);
			String lastDateNumber = lastDate.replaceAll("[^0-9]", "");

			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("startDateItem_dateTextField")));
			driver.findElement(By.name("startDateItem_dateTextField")).clear();
			driver.findElement(By.name("startDateItem_dateTextField")).sendKeys(lastDateNumber);

			Date today = new Date();
			String dateNow = sdf.format(today);
			String nowDateNumber = dateNow.replaceAll("[^0-9]", "");

			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("endDateTime_dateTextField")));
			driver.findElement(By.name("endDateTime_dateTextField")).clear();
			driver.findElement(By.name("endDateTime_dateTextField")).sendKeys(nowDateNumber);

			wait.until(ExpectedConditions
					.visibilityOfAllElementsLocatedBy(By.xpath("//table/tbody/tr/td[contains(text(), 'Consultar')]")));
			driver.findElement(By.xpath("//table/tbody/tr/td[contains(text(),'Consultar')]")).click();

			assertTrue(validaRelatorio(driver));

		} catch (Exception e) {
			
			System.out.println("Error: " + e);
		}

		driver.quit();
	}
	
	public static boolean validaRelatorio(WebDriver driver){
		
		return driver.getPageSource().contains("MOV9545");
	}

}
