package br.com.zatix;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class ConfigSetup {
	private static String os = System.getProperty("os.name");

	public static void setup(WebDriver driver) {
		System.out.println("Sistema Operacional:" + os);
		if (os.contains("Windows")) {
			System.setProperty("webdriver.gecko.driver",
					"C:\\Users\\felipe.pereira\\workspace\\selenium-java-3.0.1\\geckodriver.exe");
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\felipe.pereira\\workspace\\selenium-java-3.0.1\\ChromeDriver.exe");
		} else {
			System.setProperty("webdriver.gecko.driver", "/usr/lib/geckodriver");
		}
	}

	public static WebDriver getCurrentDriver() {
		return new HtmlUnitDriver();
	}
	
}
