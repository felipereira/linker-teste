package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class CancelaCadastroMotorista {

	public static void testaCancelarCadastroMotorista(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		try {
			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
					By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']")));
			driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/openMenu.png']"))
					.click();

			Thread.sleep(2000);
			wait.until(ExpectedConditions
					.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Motoristas')]")));
			driver.findElement(By.xpath("//td//div[contains(text(),'Motoristas')]")).click();

			wait.until(ExpectedConditions
					.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(text(),'Consultar')]")));
			driver.findElement(By.xpath("//div[contains(text(),'Consultar')]")).click();

			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("defaultButton")));
			driver.findElement(By.className("defaultButton")).click();

			boolean validate = driver.getCurrentUrl().contains("#main");

			assertTrue(validate);

			driver.quit();

		} catch (Exception e) {
			System.out.println("Error " + e);
		}
	}

}
