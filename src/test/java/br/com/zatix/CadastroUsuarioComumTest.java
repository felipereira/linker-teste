package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CadastroUsuarioComumTest {

	private static WebDriver driver;

	@Dado("^o endereco do portal omniweb$")
	public void Ambiente() {

		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o usuario logar no portal$")
	public void acessoPortal() throws InterruptedException {

		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());

	}

	@Entao("^o usuario podera criar o usuario \"(.*)\" entrado no menu Usuarios$")
	public void testaCadastroUsuarioComum(String nome) throws Exception {

		CadastroUsuarioComum.testaCadastroUsuarioComum(nome, driver);

	}
}
