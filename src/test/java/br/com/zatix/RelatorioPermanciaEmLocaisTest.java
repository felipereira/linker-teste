package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class RelatorioPermanciaEmLocaisTest {

	private static WebDriver driver;

	@Dado("^acesso ao portal de rastreamento Omniweb$")
	public void configSetup() {
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^realizado o acesso$")
	public void acessoPortal() {
		LinkerPage.visita(driver);
		LoginValida validate = new LoginValida(driver);
		assertTrue(validate.isValida());
	}

	@Entao("^podera ser gerado o Relatorio de Permanencia em Locais$")
	public void testaGerarRelatorio() {
		RelatorioPermanenciaEmLocais.testaGerarRelatorio(driver);
	}
}
