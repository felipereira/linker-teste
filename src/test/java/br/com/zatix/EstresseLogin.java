package br.com.zatix;

import org.openqa.selenium.WebDriver;

public class EstresseLogin {
	public static void testaEstresseLogin(WebDriver driver) throws InterruptedException {

		long start, finish, total, second;
		int count = 0;

		do {
			start = System.currentTimeMillis();

			LinkerPage.visita(driver);

			Thread.sleep(1000);

			LoginValida valida = new LoginValida(driver);
			boolean login = valida.isValida();

			if (login == true) {

				count++;

				finish = System.currentTimeMillis();

				total = finish - start;
				total = (total / 1000);
				second = total % 60;
				total = total / 60;

				System.out.println("Login " + count + " : " + total + "min :" + second + "seg");
			} else
				System.out.println("Login =" + login);

		} while (count != 10);

		driver.quit();
	}

}
