package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CancelaCadastroMotoristaTest {

	WebDriver driver;

	@Dado("^o usuario acessar o portal de rastreamento Omniweb$")
	public void ConfigSetup() {
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^acessar o portal$")
	public void acessoPortal() {
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera cancelar o cadastro do motorista clicando no botao cancelar$")
	public void testaCancelarCadastroMotorista() {
		CancelaCadastroMotorista.testaCancelarCadastroMotorista(driver);
	}

}
