package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class ExcluiMarcadorTest {
	
	private static WebDriver driver;
	
	@Dado("^o usuario ter acessado o portal Linker Rastreamento$")
	public void ConfigSetup(){
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}
	
	@Quando("^o usuario se logar no portal Linker Rastreamento$")
	public void acessoPortal(){
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}
	
	@Entao("^o usuario podera excluir o marcador de nome \"(.*)\" ao clicar no botao marcador no mapa$")
	public void testaExcluirMarcador(String name_marker){
		ExcluiMarcador.testaExcluirMarcador(name_marker, driver);
	}
}
