package br.com.zatix;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class TravaBauTest {

	WebDriver driver;

	@Dado("^o cliente acessar o portal$")
	public void configAmbiente() {
		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
	}

	@Quando("^o cliente se logar no sistema$")
	public void acessoPortal() {
		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o cliente podera selecionar o rastreador exemplo \"(.*)\" e enviar o comando \"(.*)\"$")
	public void testaTravarBau(String id, String travarBau) throws InterruptedException {

		TravaBau.testaTravarBau(id, travarBau, driver);
	}
}
