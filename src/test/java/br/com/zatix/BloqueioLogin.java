package br.com.zatix;

import org.openqa.selenium.WebDriver;
import static org.junit.Assert.assertFalse;

public class BloqueioLogin {

	public static void testaBloqueioLogin(WebDriver driver) {

		String s, letras = "ABCDE1234567890", usuario = "02884318000108";
		boolean login = false;
		int i = 0;
		char c;

		try {

			for (i = 0; i <= letras.length(); i++) {

				c = letras.charAt(i);
				s = new StringBuilder().append(c).toString();

				LoginAcesso acesso = new LoginAcesso(driver);
				LoginValida valida = acesso.visita("http://10.40.1.58:7110/portal/Login.html").autentica(usuario,
						s + "ABC" + i);
				assertFalse(valida.isValida());
			}

		} catch (Exception e) {

		} finally {

			long start, finish, total, second;

			start = System.currentTimeMillis();

			do {
				try {
					LinkerPage.visita(driver);

				} catch (Exception e) {

				} finally {
					login = driver.getCurrentUrl().endsWith("#main");

					System.out.println(login);

					if (login == true) {

						finish = System.currentTimeMillis();

						total = finish - start;
						total = (total / 1000);
						second = total % 60;
						total = total / 60;

						System.out.println("\nTentativas Login:" + i + "\n" + "Tempo de Bloqueio do Usuario: " + total
								+ "min :" + second + "seg\n" + "Usuario logado: " + usuario);
					}
				}

			} while (login != true);

		}
		driver.quit();
	}
}
