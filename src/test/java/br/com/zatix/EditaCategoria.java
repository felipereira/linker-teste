package br.com.zatix;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditaCategoria {

	public static void testaEdicaoDeMarcador(WebDriver driver, String new_marker_name) {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/markerDisable.png']"))
				.click();

		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//table/tbody/tr/td[contains(text(),'Categoria')]")));
		driver.findElement(By.xpath("//table/tbody/tr/td[contains(text(),'Categoria')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/edit.png']")));
		driver.findElement(By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/edit.png']")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
				"//div[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[4]/div/span/input")));
		driver.findElement(By
				.xpath("//div[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[4]/div/span/input"))
				.clear();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
				"//div[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[4]/div/span/input")));
		driver.findElement(By
				.xpath("//div[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[4]/div/span/input"))
				.sendKeys(new_marker_name, Keys.ENTER);

		try {

			Thread.sleep(1000);
			
			WebElement element = driver.findElement(
					By.xpath("//div[contains(@eventproxy,'isc_BaseListGrid_1_body')]/div/table/tbody/tr/td[4]/div"));
			
			String MarkerName = element.getText();
			String validate1 = MarkerName.replaceAll("[^a-z]", "");

			String validate2 = new_marker_name.replaceAll("[^a-z]", "");

			assertFalse(validate1.equals(validate2) == false);

		} catch (Exception e) {
			System.out.println("Error: " + e);
		}
		driver.quit();
	}
}
