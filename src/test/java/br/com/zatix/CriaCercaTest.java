package br.com.zatix;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CriaCercaTest {

	private static WebDriver driver;

	@Dado("^quando o usuario entrar no Portal Omniweb$")
	public void abrirBrowser() {

		ConfigSetup.setup(driver);
		driver = new ChromeDriver();
		//driver.manage().window().maximize();
	}
	
	@Quando("^ele informar o usuario/senha corretamente e clicar no botao de login$")
	public void acessoPortal() throws InterruptedException {

		LinkerPage.visita(driver);
		LoginValida valida = new LoginValida(driver);
		assertTrue(valida.isValida());
	}

	@Entao("^o usuario podera criar a cerca \"(.*)\" clicando com o botao direito no mapa$")
	public void testaCriacaoDeCerca(String nomeCerca) throws InterruptedException {

		CriaCerca.testaCriacaoDeCerca(nomeCerca, driver);
	}
}
