package br.com.zatix;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DestravaBau {

	public static void testaDestravarBau(String id, String nomeComando, WebDriver driver) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("licensePlate$148l")));

		driver.findElement(By.name("licensePlate$148l")).sendKeys(id);
		Thread.sleep(2000);

		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.cssSelector("div [aria-label='Enviar comando']")));
		driver.findElement(By.cssSelector("div [aria-label='Enviar comando']")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("commandCombo")));
		Thread.sleep(1000);
		driver.findElement(By.name("commandCombo")).clear();
		Thread.sleep(1000);
		driver.findElement(By.name("commandCombo")).sendKeys(nomeComando, Keys.TAB, Keys.ENTER, Keys.ESCAPE);

		Thread.sleep(9000);
		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//td//div[contains(text(),'Fechar')]")));
		driver.findElement(By.xpath("//td//div[contains(text(),'Fechar')]")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/showVehicleHistory.png']")));
		driver.findElement(
				By.cssSelector("img[src='http://10.40.1.58:7110/portal/img/buttons/showVehicleHistory.png']")).click();

		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div [aria-label='Filtrar']")));
		driver.findElement(By.cssSelector("div [aria-label='Filtrar']")).click();

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("multiComboBox")));
		driver.findElement(By.name("multiComboBox")).clear();
		Thread.sleep(1000);
		driver.findElement(By.name("multiComboBox")).sendKeys(nomeComando, Keys.ENTER);

		driver.quit();
	}
}
